## potato_RMX1801-userdebug 12 SP1A.210812.016 09b12aac82 test-keys
- Manufacturer: oppo
- Platform: sdm660
- Codename: RMX1801
- Brand: oppo
- Flavor: potato_RMX1801-userdebug
- Release Version: 12
- Id: SP1A.210812.016
- Incremental: 09b12aac82
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: oppo/potato_RMX1801/RMX1801:12/SP1A.210812.016/09b12aac82:userdebug/test-keys
- OTA version: 
- Branch: potato_RMX1801-userdebug-12-SP1A.210812.016-09b12aac82-test-keys
- Repo: oppo_rmx1801_dump_31834


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
